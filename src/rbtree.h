#ifndef _RBTREE_H_
#define _RBTREE_H_

#include "common.h"

/*
 * USAGE
 *  struct rb_tree_t example_root;
 *
 *  # Init the root
 *  rb_tree_init(&example_root, cmp_func, delete_func, key_len, key_offset);
 *
 *  # rb node with payload
 *  struct example_t {
 *      RB_TREE_NODE;
 *      key;
 *      ;
 *      ;
 *      ;
 *      };
 *  struct example_t example_node_p;
 *
 *  rb_tree_insert(&example_root,
 *                  (struct rb_tree_node_p *)example_node_p);
 */

#define RB_TREE_RED 1
#define RB_TREE_BLACK 2
#define GPARENT(N) (N)->_pnode_->_pnode_
#define PARENT(N) (N)->_pnode_

#define RB_TREE_INIT(T, CMP, K_LEN, K_OFF)                      \
                                (T)->root = RB_TREE_LEAF;       \
                                (T)->compare = CMP;             \
                                (T)->rb_tree_free = NULL;       \
                                (T)->n_cnt = 0;                 \
                                (T)->k_off = K_OFF;             \
                                (T)->k_len = K_LEN;             \
                                (T)->color = RB_TREE_BLACK

#define RB_TREE_INITIALIZE { RB_TREE_LEAF, NULL, NULL,  \
                                0, 0, 0, RB_TREE_BLACK }

#define RB_TREE(N)  struct rb_tree_t (N)
#define RB_TREE_EXTERN(N)  extern struct rb_tree_t (N)

#define RB_TREE_KEY(T,N)    \
            (T)->k_off + (uint8_t *)(N)

#define RB_TREE_COMPARE(T, K, N)   \
    (T)->compare(K, RB_TREE_KEY(T,N), (T)->k_len)

#define RB_TREE_NODE_COMPARE(T, N1, N2)   \
    (T)->compare(RB_TREE_KEY(T,N1), RB_TREE_KEY(T,N2), (T)->k_len)

#define RB_TREE_NODE                \
    struct rb_tree_node_t *_pnode_;   \
    struct rb_tree_node_t *_lnode_;   \
    struct rb_tree_node_t *_rnode_;   \
    uint8_t color;                  \
    uint8_t _RB_TREE_NODE_PAD_[7]

#define RB_TREE_LEAF  &_nnode_

#define RB_KEY_OFF(S, K, OFF)   \
    OFF = 0;                    \
    OFF = OFFSET(S, K)

#define RB_MKEY_OFF(S, K, N, OFF)   \
    OFF = 0;                        \
    OFF = OFFSET(S, K);             \
    OFF = OFF -OFFSET(S, N)

#define RB_NODE(N)  struct rb_tree_node_t (N)

struct rb_tree_node_t {
    RB_TREE_NODE;
};

struct rb_tree_t {
    struct rb_tree_node_t *root;
    char (*compare)(const void *, const void *, size_t);
    void (*rb_tree_free)(struct rb_tree_node_t *);
    uint32_t n_cnt;
    uint8_t k_off;
    uint8_t k_len;
    uint8_t color;
    uint8_t _RB_TREE_TREE_PAD_;
};

extern struct rb_tree_node_t _nnode_;

typedef enum {
    RBT_ERROR = -1,
    RBT_SUCCESS = 0,
    RBT_SRCH_FOUND = 1,
    RBT_EMPTY = 2,
}RBT_RET;

RBT_RET rb_tree_init (struct rb_tree_t *, uint32_t , uint32_t ,
                        char (func) (const void *, const void *, size_t ));
char rb_tree_u32_compare (const void *, const void *, size_t);
char rb_tree_u16_compare (const void *, const void *, size_t);
RBT_RET rb_tree_search (struct rb_tree_t *, const void *k, struct rb_tree_node_t **);
RBT_RET rb_tree_insert (struct rb_tree_t *, struct rb_tree_node_t *,
                                                struct rb_tree_node_t *);
RBT_RET rb_tree_delete (struct rb_tree_t *, struct rb_tree_node_t *);
struct rb_tree_node_t * rb_tree_min (struct rb_tree_node_t *);
struct rb_tree_node_t * rb_tree_successor (struct rb_tree_node_t *);

// todo
int rb_tree_inorder_walk (struct rb_tree_node_t *);
struct rb_tree_node_t * rb_tree_bfs_walk (struct rb_tree_node_t *);
struct rb_tree_node_t * rb_tree_predecessor (struct rb_tree_node_t *);
int rb_tree_height (struct rb_tree_node_t *);

#endif /* _RBTREE_H_ */
