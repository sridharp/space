#include "rbtree.h"

struct rb_tree_node_t _nnode_ =
                    {NULL,
                     NULL,
                     NULL,
                     RB_TREE_BLACK };

static void
_rb_tree_rleft(struct rb_tree_t *tree_p,
                struct rb_tree_node_t *node_p)
{
    struct rb_tree_node_t *n_p;

    /* Cormen Section 14.2 */
    n_p = node_p->_rnode_;
    node_p->_rnode_ = n_p->_lnode_;
    if (n_p->_lnode_ != RB_TREE_LEAF) {
        n_p->_lnode_->_pnode_ = node_p;
    }

    if (n_p != RB_TREE_LEAF) {
        n_p->_pnode_ = PARENT(node_p);
    }

    if (PARENT(node_p)) {
        if (node_p == PARENT(node_p)->_lnode_) {
            PARENT(node_p)->_lnode_ = n_p;
        } else {
            PARENT(node_p)->_rnode_ = n_p;
        }
    }
    else {
        tree_p->root = n_p;
    }

    n_p->_lnode_ = node_p;
    if (node_p != RB_TREE_LEAF)
      PARENT(node_p) = n_p;
}

static void
_rb_tree_rright(struct rb_tree_t *tree_p,
                struct rb_tree_node_t *node_p)
{
    struct rb_tree_node_t *n_p;

    /* Cormen Section 14.2 */
    n_p = node_p->_lnode_;
    node_p->_lnode_ = n_p->_rnode_;
    if (n_p->_rnode_ != RB_TREE_LEAF) {
        n_p->_rnode_->_pnode_ = node_p;
    }

    if (n_p != RB_TREE_LEAF) {
        n_p->_pnode_ = PARENT(node_p);
    }

    if (PARENT(node_p)) {
        if (node_p == PARENT(node_p)->_rnode_) {
            PARENT(node_p)->_rnode_ = n_p;
        } else {
            PARENT(node_p)->_lnode_  = n_p;
        }
    } else {
        tree_p->root = n_p;
    }
    n_p->_rnode_ = node_p;
    if (node_p != RB_TREE_LEAF) {
        PARENT(node_p) = n_p;
    }
}

RBT_RET
rb_tree_init(struct rb_tree_t *root_p, uint32_t k_len, uint32_t k_off,
                char (func) (const void *k1, const void *k2, size_t ksize))
{
    if (root_p && (root_p == (struct rb_tree_t *)RB_TREE_LEAF))
        return RBT_ERROR;

    if (func == NULL) {
        RB_TREE_INIT(root_p, rb_tree_u32_compare, k_len, k_off);
    } else {
        RB_TREE_INIT(root_p, func, k_len, k_off);
    }

    return RBT_SUCCESS;
}

char
rb_tree_u32_compare(const void *k1, const void *k2, size_t ksize)
{
    uint32_t a = *(uint32_t *)k1;
    uint32_t b = *(uint32_t *)k2;

    UNUSED(ksize);
    if (a == b)
        return 0;

    if (a < b)
        return -1;
    else
        return 1;
}

char
rb_tree_u16_compare(const void *k1, const void *k2, size_t ksize)
{
    uint16_t a = *(uint16_t *)k1;
    uint16_t b = *(uint16_t *)k2;

    UNUSED(ksize);
    if (a == b)
        return 0;

    if (a < b)
        return -1;
    else
        return 1;
}

/*
 * @ rb_tree_search
 * @ psudo  [regular BST search]
 *      if found
 *          *result_pp found node;
 *          RET = FOUND
 *      else
 *          *result_pp = lost_node {last node on the way to key before grounded}
 *          if key > lost_node
 *              RET = RIGHT
 *          else
 *              RET = LEFT
 */
RBT_RET
rb_tree_search(struct rb_tree_t *tree_p, const void *k,
                struct rb_tree_node_t **result_pp)
{
    struct rb_tree_node_t *r_p = NULL;
    RBT_RET res;

    *result_pp = NULL;
    if (!tree_p)
        return RBT_ERROR;

    if (!tree_p->n_cnt)
        return RBT_EMPTY;

    r_p = tree_p->root;
    while (r_p && (r_p != RB_TREE_LEAF)) {
        *result_pp = r_p;
        if ((res = RB_TREE_COMPARE(tree_p, k, r_p)) == 0)
            return RBT_SRCH_FOUND;

        if (res == -1)
            r_p = r_p->_lnode_;
        else
            r_p = r_p->_rnode_;
    }
    return RBT_SUCCESS;
}

/*
 * @rb_tree_insert
 * @RB(NOT ROOT BEER) property
 *      1. All nodes MUST have one or other color
 *      2. Root and all the leaf(i.e the ground nodes) MUST be black
 *      3. Parent of the every RED node MUST be BLACK
 *      4. Black-height(ref knuth) for any NODE in the tree to ALL the possible
 *         leaf(i.e gound nodes) MUST be SAME.
 *
 */
RBT_RET
rb_tree_insert(struct rb_tree_t *tree_p, struct rb_tree_node_t *node_p,
                struct rb_tree_node_t *parent)
{
    struct rb_tree_node_t *uncle_p;
    RBT_RET res;

    PARENT(node_p) = parent;
    node_p->_lnode_ = RB_TREE_LEAF;
    node_p->_rnode_ = RB_TREE_LEAF;
    node_p->color = RB_TREE_RED;

    /* BST insertion */
    if (!parent) {
       rb_tree_search(tree_p, RB_TREE_KEY(tree_p, node_p) , &parent);
       if (!parent)
            tree_p->root = node_p;
    } else {
        res = RB_TREE_NODE_COMPARE(tree_p, parent, node_p);
        if (res == 0)
            return RBT_ERROR;
        if (res == -1)
            parent->_rnode_ = node_p;
        else
            parent->_lnode_ = node_p;
    }
    tree_p->n_cnt++;

    /* RB balancing act */
    while ((node_p != tree_p->root) &&
           (PARENT(node_p)->color == RB_TREE_RED)) {
        /* RULE 3 violation */
        /*
           -> violation can ONLY possible when the node have parent
           and grand parent.
           -> node_p will always be RED within this iterator
        */
        if (PARENT(node_p) == GPARENT(node_p)->_lnode_) {
            /* CASE A */
            uncle_p = GPARENT(node_p)->_rnode_;
            if (uncle_p->color == RB_TREE_RED) {
                /* CASE A.1 */
                PARENT(node_p)->color = RB_TREE_BLACK;
                uncle_p->color = RB_TREE_BLACK;
                GPARENT(node_p)->color = RB_TREE_RED;
                node_p = GPARENT(node_p);
            } else {
                if (node_p == PARENT(node_p)->_rnode_) {
                    /* CASE A.2 */
                    node_p = PARENT(node_p);
                    _rb_tree_rleft(tree_p, node_p);
                }
                /* CASE A.3 (also the terminator) */
                PARENT(node_p)->color = RB_TREE_BLACK;
                GPARENT(node_p)->color = RB_TREE_RED;
                _rb_tree_rright(tree_p, GPARENT(node_p));
            }
        } else {
            /* CASE B (mirror symetric to CASE A)*/
            uncle_p = GPARENT(node_p)->_lnode_;
            if (uncle_p->color == RB_TREE_RED) {
                /* CASE B.1 */
                PARENT(node_p)->color = RB_TREE_BLACK;
                uncle_p->color = RB_TREE_BLACK;
                GPARENT(node_p)->color = RB_TREE_RED;
                node_p = GPARENT(node_p);
            } else {
                if (node_p == PARENT(node_p)->_lnode_) {
                    /* CASE B.2 */
                    node_p = PARENT(node_p);
                    _rb_tree_rright(tree_p, node_p);
                }
                /* CASE B.3 (also the terminator) */
                PARENT(node_p)->color = RB_TREE_BLACK;
                GPARENT(node_p)->color = RB_TREE_RED;
                _rb_tree_rleft(tree_p, GPARENT(node_p));
            }
        }
    }
    tree_p->root->color = RB_TREE_BLACK;
    return RBT_SUCCESS;
}

RBT_RET
rb_tree_delete(struct rb_tree_t *tree_p,
                struct rb_tree_node_t *del_p)
{
    struct rb_tree_node_t *sib_p, *node_p, *y_p;
    uint8_t color;

    /* BST delete */
    /* Cormen section 13.3 */
    if ((del_p->_lnode_ == RB_TREE_LEAF) ||
        (del_p->_rnode_ == RB_TREE_LEAF)) {
        y_p = del_p;
    } else {
        y_p = del_p->_rnode_;
        /* del_p successor */
        while (y_p->_lnode_ != RB_TREE_LEAF)
            y_p = y_p->_lnode_;
    }

    if (y_p->_lnode_ != RB_TREE_LEAF)
        node_p = y_p->_lnode_;
    else
        node_p = y_p->_rnode_;

    PARENT(node_p) = y_p->_pnode_;
    if (y_p->_pnode_) {
        if (y_p == y_p->_pnode_->_lnode_) {
            y_p->_pnode_->_lnode_  = node_p;
        } else {
            y_p->_pnode_->_rnode_ = node_p;
        }
    } else {
        tree_p->root = node_p;
    }

    color = y_p->color;
    if (y_p != del_p) {
        /* delete node with 2 child case */
        /* replace del_p with y_p */
        del_p->_rnode_->_pnode_ = y_p;
        del_p->_lnode_->_pnode_ = y_p;
        y_p->_lnode_ = del_p->_lnode_;
        y_p->_rnode_ = del_p->_rnode_;
        y_p->_pnode_ = del_p->_pnode_;
        y_p->color = del_p->color;

        if (del_p->_pnode_) {
            if (del_p == del_p->_pnode_->_lnode_) {
                del_p->_pnode_->_lnode_  = y_p;
            } else {
                del_p->_pnode_->_rnode_ = y_p;
            }
        } else {
            tree_p->root = y_p;
        }
    }

    /* Red-black balancing act */
    if (color == RB_TREE_BLACK) {
        while (node_p != tree_p->root && node_p->color == RB_TREE_BLACK) {
            if (node_p == PARENT(node_p)->_lnode_) {
                sib_p = PARENT(node_p)->_rnode_;
                if (sib_p->color == RB_TREE_RED) {
                    sib_p->color = RB_TREE_BLACK;
                    PARENT(node_p)->color = RB_TREE_RED;
                    _rb_tree_rleft(tree_p, PARENT(node_p));
                    sib_p = PARENT(node_p)->_rnode_;
                }

                if ((sib_p->_lnode_->color == RB_TREE_BLACK) &&
                    (sib_p->_rnode_->color == RB_TREE_BLACK)) {
                    sib_p->color = RB_TREE_RED;
                    node_p = PARENT(node_p);
                } else {
                    if (sib_p->_rnode_->color == RB_TREE_BLACK) {
                        sib_p->_lnode_->color = RB_TREE_BLACK;
                        sib_p->color = RB_TREE_RED;
                        _rb_tree_rright(tree_p, sib_p);
                        sib_p = PARENT(node_p)->_rnode_;
                    }
                    sib_p->color = PARENT(node_p)->color;
                    PARENT(node_p)->color = RB_TREE_BLACK;
                    sib_p->_rnode_->color = RB_TREE_BLACK;
                    _rb_tree_rleft(tree_p, PARENT(node_p));
                    node_p = tree_p->root;
                }
            } else {
                sib_p = PARENT(node_p)->_lnode_;
                if (sib_p->color == RB_TREE_RED) {
                    sib_p->color = RB_TREE_BLACK;
                    PARENT(node_p)->color = RB_TREE_RED;
                    _rb_tree_rright(tree_p, PARENT(node_p));
                    sib_p = PARENT(node_p)->_lnode_;
                }

                if ((sib_p->_rnode_->color == RB_TREE_BLACK) &&
                    (sib_p->_lnode_->color == RB_TREE_BLACK)) {
                    sib_p->color = RB_TREE_RED;
                    node_p = PARENT(node_p);
                } else {
                    if (sib_p->_lnode_->color == RB_TREE_BLACK) {
                        sib_p->_rnode_->color = RB_TREE_BLACK;
                        sib_p->color = RB_TREE_RED;
                        _rb_tree_rleft(tree_p, sib_p);
                        sib_p = PARENT(node_p)->_lnode_;
                    }
                    sib_p->color = PARENT(node_p)->color;
                    PARENT(node_p)->color = RB_TREE_BLACK;
                    sib_p->_lnode_->color = RB_TREE_BLACK;
                    _rb_tree_rright(tree_p, PARENT(node_p));
                    node_p = tree_p->root;
                }
            }
        }
        node_p->color = RB_TREE_BLACK;
    }
    return RBT_SUCCESS;
}

struct rb_tree_node_t *
rb_tree_min(struct rb_tree_node_t *root_p)
{

    if (root_p == RB_TREE_LEAF)
        return NULL;

    while (root_p->_lnode_ != RB_TREE_LEAF)
        root_p = root_p->_lnode_;

    return root_p;
}

struct rb_tree_node_t *
rb_tree_successor(struct rb_tree_node_t *node_p)
{
    struct rb_tree_node_t *p_p;

    if (node_p == RB_TREE_LEAF)
        return NULL;

    if (node_p->_rnode_ != RB_TREE_LEAF) {
        /* MIN_NODE from the right subtree */
        node_p = node_p->_rnode_;
        while (node_p->_lnode_ != RB_TREE_LEAF)
            node_p = node_p->_lnode_;
        return node_p;
    } else {
        /* first ancester who is a left node */
        p_p = PARENT(node_p);
        while ((p_p) && (p_p != RB_TREE_LEAF) && (node_p == p_p->_rnode_)) {
            node_p = p_p;
            p_p = PARENT(node_p);
        }

        if (p_p == RB_TREE_LEAF)
            return NULL;

        return (p_p);
    }
}

int
rb_tree_height(struct rb_tree_node_t *node_p)
{
    if (!node_p)
        return 0;
    return 1 + (MAX(rb_tree_height(node_p->_lnode_),  rb_tree_height(node_p->_rnode_)));
}

int
rb_tree_inorder_walk(struct rb_tree_node_t *node_p)
{
    if(!node_p)
        return 0;
    //TODO
    return 0;
}


//rb_tree_bfs_walk()
//rb_tree_predecessor()
