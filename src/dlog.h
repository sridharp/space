#ifndef _DLOG_H_
#define _DLOG_H_

#include <syslog.h>

#define LOG printf

typedef enum {
    DL_RET_ERROR = -1,
    DL_RET_SUCCESS = 0,
}DL_RET;

struct dev_op_entry {
    RB_TREE_NODE;
    uint32_t key;
    uint16_t dev_id;
    uint8_t op;
    //struct tm time;
    uint32_t time;
    uint32_t e_num;
    int log_fd;
};

#define DEV_OP_KEY(d, o) ((((d) & 0xffff) <<16) | (((o) & 0xff) <<8))
#define DEV_KEY(d) (((d) & 0xffff) <<16)
#define DEV_MASK (0xffff0000)
#define OP_MASK (0xff00)

struct lsysd {
    struct rb_tree_t dev_op_tree;
};

struct l_event {
    uint8_t op;
    uint16_t dev_id;
    //struct tm time;
    uint32_t time;
    uint8_t payload[0];
};

/* Device log API */
struct lsysd * dlog_init();
DL_RET dlog_start();
DL_RET dlog_log_event(struct lsysd *, struct l_event *);
DL_RET dlog_node_list(struct lsysd *);
DL_RET dlog_shut(struct lsysd *);
DL_RET dlog_inorder_walk(struct rb_tree_node_t *, void (*)(struct dev_op_entry *));
DL_RET dlog_dev_op_list(struct dev_op_entry *);
DL_RET dlog_dev_op_list_all(struct lsysd *);

#endif /* _DLOG_H_ */
