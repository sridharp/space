#ifndef _COMMON_H__
#define _COMMON_H__

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

#define TRUE 1
#define FALSE 0

#define MIN(A,B) \
    (A) < (B) ? (A) : (B)

#define MAX(A,B) \
    (A) > (B) ? (A) : (B)

#define OFFSET(S, M) ((size_t) &(((S *) 0)->M))
#define UNUSED(x) (void)(x)

#ifndef NULL
#define NULL (void *) 0
#endif

#endif
