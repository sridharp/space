#include "t_ut.h"

int
ut_rbtree()
{
    uint32_t x, key;
    uint32_t height, node;
    RBT_RET rc;
    struct t_entry_t *ent_p = NULL;
    struct rb_tree_node_t *res_p = NULL;
    struct rbt_ut_t *ut_p;

    UNUSED(rc);
    ut_p = (struct rbt_ut_t *)malloc(sizeof(struct rbt_ut_t) * CNT);
    rb_tree_init(&g_rb_t, sizeof(uint32_t),
                    OFFSET(struct t_entry_t, key), NULL);

    /* insert testing */
    for(x = 0; x < CNT; x++) {
        key = random();
        rc = rb_tree_search(&g_rb_t, (const void *)&(key), &res_p);

        if (rc == RBT_ERROR) {
            LOG("IT IS ERROR \n");
            assert(!(rc == RBT_ERROR));
        }
        if (rc != RBT_SRCH_FOUND) {
            ent_p = malloc(sizeof(struct t_entry_t));
            memset(ent_p, 0, sizeof(struct t_entry_t));
            ent_p->key = key;

            /* Unit testing the entry */
            (ut_p + x)->key = key;
            (ut_p + x)->node_p = ent_p;

            rb_tree_insert(&g_rb_t, (void *)ent_p, (void *)res_p);
            LOG("Insert successfull for node (%p) key (0x%x)\n",ent_p,
                    ent_p->key);
        } else {
            LOG("Random is not random after all. Key (0x%x)\n", key);
        }
    }
    LOG("\n\nInserting success\n\n");
    node = g_rb_t.n_cnt;
    height = rb_tree_height(g_rb_t.root);

    /* search testing */
    for(x = 0; x < CNT; x++) {
        rc = rb_tree_search(&g_rb_t, (const void *)&((ut_p + x)->key), &res_p);
        if ((rc != RBT_SRCH_FOUND) &&
            ((ut_p + x)->node_p != (struct t_entry_t *)res_p)){
            LOG("ASSERTING %d %d %p %p\n", rc, x, (ut_p + x)->node_p, res_p);
            goto BAD;
        } else {
            LOG("Search successfull for node (%p) key (0x%x)\n", res_p,
                    ((struct t_entry_t *)res_p)->key);

            rc = rb_tree_delete(&g_rb_t, res_p);
            if (rc != RBT_SUCCESS)
                goto BAD;

            LOG("Delete successfull for node (%p) key (0x%x)\n", res_p,
                    ((struct t_entry_t *)res_p)->key);

            free(res_p);
        }
    }

    free(ut_p);
    LOG("Max Height of the tree: 0x%x\n", height);
    LOG("Number of node in the tree: 0x%x\n", node);
    LOG("Testing success\n");
    return 0;
BAD:
    LOG("ASSERTING ..\n");
    assert(FALSE);
    return -1;
}
