#include "t_ut.h"

RB_TREE(g_rb_t);
struct lsysd *g_sysd_p;

static int
_ut_rbtree()
{
    int rt;

    rt = ut_rbtree();
    if (rt < 0) {
        LOG("RB tree FAILLED\n");
        return -1;
    } else
        LOG("RB tree PASSED\n");

    return rt;
}

int
main()
{
    int rt, op;

    g_sysd_p = dlog_init();
    if (!g_sysd_p)
        return -1;

    while (scanf("%x", &op)) {
        printf("Option is %x \n", op);
        if (op == 0)
            return 0;
        switch (op) {
          case 1:
            /* simulate dev log */
            LOG("Simulate dev log\n");
            rt = ut_dlog_sim(50000);
            break;
          case 2:
            /* check g_sys_d */
            ut_show_sysd();
            break;
          case 3:
            /* walk the dev_log tree */
            dlog_node_list(g_sysd_p);
            break;
          case 4:
            /* walk the dev_log tree */
            dlog_dev_op_list_all(g_sysd_p);
            break;
          case 5:
            /* ut rbtree */
            rt = _ut_rbtree();
            break;
          default:
            LOG("BAD_OPTION\n");
        }
    }
}
