#ifndef _T_UT_
#define _T_UT_

#include "../common.h"
#include "../rbtree.h"
#include "../dlog.h"

#define LOG printf
#define CNT 1024*10

/* RB tree UT */
struct t_entry_t {
    RB_TREE_NODE;
    uint32_t key;
    uint32_t payload;
};

struct rbt_ut_t {
    uint32_t key;
    struct t_entry_t *node_p;
};

/* device log UT */
extern struct lsysd *g_sysd_p;
int ut_dlog_sim(uint32_t);
int ut_show_sysd();

/* rbtree UT */
RB_TREE_EXTERN(g_rb_t);
int ut_rbtree();

#endif /* _T_UT_ */
