#include "t_ut.h"

#define RAND_OP     (random() % 0xff)
#define RAND_DEV    (random() % 0xffff)
#define RAND_TIME   (random() % 0xff)

#define OP_CNT 1000

int
ut_dlog_sim(uint32_t e_cnt)
{
    struct l_event *event_p;
    uint32_t d_cnt, o_cnt;
    int rt;

    event_p = (struct l_event *)malloc(sizeof(struct l_event));
    /* for UT lets have 50 events per node */
    for (d_cnt = 0; d_cnt <= e_cnt/OP_CNT; d_cnt++) {
        event_p->dev_id = RAND_DEV;
        for (o_cnt = 0; o_cnt <= OP_CNT; o_cnt++) {
            event_p->op = RAND_OP;
            event_p->time = RAND_TIME;
            LOG("0x%x, 0x%x, 0x%x\n",event_p->dev_id, event_p->op, event_p->time);
            rt = dlog_log_event(g_sysd_p, event_p);
            if (rt < 0)
                LOG("Dev operation event failled: %d \n", rt);
        }
    }
    free(event_p);
    return 0;
}

int
ut_show_sysd()
{
    LOG("Number of nodes in the tree: 0x%x\n", g_sysd_p->dev_op_tree.n_cnt);
    LOG("Height of the tree: 0x%x\n", rb_tree_height(g_sysd_p->dev_op_tree.root));
    return 0;
}
