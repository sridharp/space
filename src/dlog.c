#include "common.h"
#include "rbtree.h"
#include "dlog.h"

static void
_list_dev(struct dev_op_entry *node_p)
{
    if ((node_p->key & DEV_MASK) == node_p->key) {
         LOG("node key: 0x%x time: 0x%x dev :0x%x op: 0x%x \n ",
                node_p->key,
                node_p->time,
                node_p->dev_id,
                node_p->op);
    }
}

static void
_list_dev_op(struct dev_op_entry *node_p)
{
    if ((node_p->key & DEV_MASK) == node_p->key) {
         LOG("node key: 0x%x time: 0x%x dev :0x%x op: 0x%x \n ",
                node_p->key,
                node_p->time,
                node_p->dev_id,
                node_p->op);
        dlog_dev_op_list(node_p);
    }
}

static DL_RET
dl_log_update(struct rb_tree_t *tree_p, struct dev_op_entry *entry_p)
{
    struct dev_op_entry *dev_op, *dev_p;
    struct rb_tree_node_t *res_p = NULL, *r2_p = NULL;
    uint32_t key;
    RBT_RET rt;

    rt = rb_tree_search(tree_p, (const void *)(&(entry_p->key)), &res_p);

    if (rt == RBT_ERROR)
        return DL_RET_ERROR;

    if (rt == RBT_SRCH_FOUND) {
        /* update the entry */

        key = DEV_KEY(entry_p->dev_id);
        if (rb_tree_search(tree_p, (const void *)(&key), &r2_p) != RBT_SRCH_FOUND) {
            /* Error NULL-op dev is missing, when dev_op is added.
             * Delete the dev-op and return error */
            rb_tree_delete(tree_p, res_p);
            return DL_RET_ERROR;
        }
        dev_op = (struct dev_op_entry *)res_p;
        dev_op->e_num ++;
        dev_op->time += entry_p->time;
        dev_p = (struct dev_op_entry *)r2_p;
        dev_p->time += entry_p->time;
        dev_p->e_num++;
    } else {
        /* new dev-op node */
        dev_op = malloc(sizeof(struct dev_op_entry));
        memset(dev_op, 0, sizeof(struct dev_op_entry));
        dev_op->key = DEV_OP_KEY(entry_p->dev_id, entry_p->op);
        dev_op->dev_id = entry_p->dev_id;
        dev_op->op = entry_p->op;
        dev_op->time += entry_p->time;
        dev_op->e_num++;
        assert(dev_op->key);
        rt = rb_tree_insert(tree_p, (void *)dev_op, res_p);
        if (rt == RBT_ERROR)
            return DL_RET_ERROR;

        /* check if the device node is present else add dev node */
        key = DEV_KEY(entry_p->dev_id);
        res_p = NULL;
        rt = rb_tree_search(tree_p, (const void *)(&(key)), &res_p);
        if (rt == RBT_SRCH_FOUND) {
            dev_p = (struct dev_op_entry *)res_p;
            dev_p->time += entry_p->time;
            dev_p->e_num++;
        } else {
            dev_p = malloc(sizeof(struct dev_op_entry));
            memset(dev_p, 0, sizeof(struct dev_op_entry));
            dev_p->key = DEV_KEY(entry_p->dev_id);
            dev_p->dev_id = entry_p->dev_id;
            dev_p->op = 0;
            dev_p->time += entry_p->time;
            dev_p->e_num++;
            assert(dev_p->key);
            rt = rb_tree_insert(tree_p, (struct rb_tree_node_t *)dev_p, res_p);
            if (rt == RBT_ERROR)
                return DL_RET_ERROR;
        }
    }
    return DL_RET_SUCCESS;
}

struct lsysd*
dlog_init()
{
    struct lsysd *d_p = malloc(sizeof(struct lsysd));

    if (!d_p)
        return NULL;

    rb_tree_init(&(d_p->dev_op_tree), sizeof(uint32_t),
             OFFSET(struct dev_op_entry, key), NULL);

    return d_p;
}

DL_RET
dlog_log_event(struct lsysd *dl_p, struct l_event *event_p)
{
    struct dev_op_entry entry;

    if (!dl_p || !event_p) {
        if (!dl_p)
            syslog(LOG_ERR, "System not init");
        if (!event_p)
            syslog(LOG_ERR, "NULL event update");
        return DL_RET_ERROR;
    }

    entry.dev_id = event_p->dev_id;
    entry.op = event_p->op;
    entry.key = DEV_OP_KEY(event_p->dev_id, event_p->op);
    entry.time = event_p->time;

    return dl_log_update(&(dl_p->dev_op_tree), &entry);
}

DL_RET
dlog_inorder_walk(struct rb_tree_node_t *node_p, void (*func)(struct dev_op_entry *))
{
    struct dev_op_entry *dl_p;

    if (!node_p || !func)
        return DL_RET_SUCCESS;

    if (node_p->_lnode_)
        dlog_inorder_walk(node_p->_lnode_, func);

    dl_p = (struct dev_op_entry *)node_p;

    if (dl_p->key) {
        func(dl_p);
    }

    if (node_p->_rnode_)
        dlog_inorder_walk(node_p->_rnode_, func);

    return DL_RET_SUCCESS;
}

DL_RET
dlog_node_list(struct lsysd *dl_p)
{
    dlog_inorder_walk(dl_p->dev_op_tree.root, _list_dev);
    return DL_RET_SUCCESS;
}

DL_RET
dlog_dev_op_list(struct dev_op_entry *dev_p)
{
    struct dev_op_entry *op_p = NULL;

    if (!dev_p)
        return DL_RET_ERROR;

    LOG("OP of dev: 0x%x\n", dev_p->key);
    op_p = (struct dev_op_entry *)rb_tree_successor((struct rb_tree_node_t *)dev_p);
    while (op_p && ((op_p->key & DEV_MASK) == dev_p->key)) {
        LOG("\t\tDEV: 0x%x OP: 0x%x time: 0x%x event_cnt: 0x%x\n",
                                                    op_p->dev_id,
                                                    op_p->op,
                                                    op_p->time,
                                                    op_p->e_num);
        op_p = (struct dev_op_entry *)rb_tree_successor((struct rb_tree_node_t *)op_p);
    }
    return DL_RET_SUCCESS;
}

DL_RET
dlog_dev_op_list_all(struct lsysd *dl_p)
{
    dlog_inorder_walk(dl_p->dev_op_tree.root, _list_dev_op);
    return DL_RET_SUCCESS;
}
