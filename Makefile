PROJ_ROOT := $(CURDIR)
PROJ_BUILD := $(CURDIR)/build
MAXCPU=$(shell grep -c '^processor' /proc/cpuinfo)
export PROJ_ROOT
export PROJ_BUILD
export MAXCPU

MAKE=make -j$(MAXCPU)
ECHO=@echo

.PHONY: sanity all lib space

all: sanity lib space

sanity:
ifndef PROJ_ROOT
	$(error PROJ_ROOT is not set)
endif
ifndef PROJ_BUILD
	$(error PROJ_BUILD is not set)
endif

lib: sanity
	$(ECHO) "BUILDING LOCKLESS RING"
	$(MAKE) config T=x86_64-native-linuxapp-gcc -C $(PROJ_ROOT)/lib/dpdk
	$(MAKE) -C $(PROJ_ROOT)/lib/dpdk

space:
	$(ECHO) "BUILDING SPACE PROJECT"
	$(MAKE) -C ./src

clean_space: sanity
	$(ECHO) "CLEANING BUILD"
	$(MAKE) clean -C ./src

clean_lib: sanity
	$(ECHO) "CLEANING LOCKLESS RING LIB"
	$(MAKE) clean -C $(PROJ_ROOT)/lib/dpdk

clean_all: sanity clean_space clean_lib

check: 
	$(ECHO) "UNIT TESTING "
	$(MAKE) check -C ./src
